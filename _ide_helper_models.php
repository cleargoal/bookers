<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Author
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Author newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Author newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Author query()
 */
	class Author extends \Eloquent {}
}

namespace App{
/**
 * App\Book
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book query()
 */
	class Book extends \Eloquent {}
}

namespace App{
/**
 * App\Category
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category query()
 */
	class Category extends \Eloquent {}
}

namespace App{
/**
 * App\Genre
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Genre newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Genre newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Genre query()
 */
	class Genre extends \Eloquent {}
}

namespace App{
/**
 * App\PropsType
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropsType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropsType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropsType query()
 */
	class PropsType extends \Eloquent {}
}

namespace App{
/**
 * App\PropsTypeValue
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropsTypeValue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropsTypeValue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropsTypeValue query()
 */
	class PropsTypeValue extends \Eloquent {}
}

namespace App{
/**
 * App\Tag
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tag query()
 */
	class Tag extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 */
	class User extends \Eloquent {}
}

