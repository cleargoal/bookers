<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('category_id')->index();
            $table->string('name')->index();
            $table->string('annotation');
            $table->string('isbn')->index();
            $table->bigInteger('publisher_id')->index();
            $table->string('publish_year')->index();
            $table->string('citation');
            $table->string('pages');
            $table->string('chapters');
            $table->string('picture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book');
    }
}
