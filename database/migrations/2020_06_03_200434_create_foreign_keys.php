<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('props_type_values', function(Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('props_types')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::table('props_type_values', function(Blueprint $table) {
            $table->foreign('book_id')->references('id')->on('book')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::table('book_tag', function(Blueprint $table) {
            $table->foreign('book_id')->references('id')->on('book')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::table('book_tag', function(Blueprint $table) {
            $table->foreign('tag_id')->references('id')->on('tags')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::table('book_genre', function(Blueprint $table) {
            $table->foreign('book_id')->references('id')->on('book')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::table('book_genre', function(Blueprint $table) {
            $table->foreign('genre_id')->references('id')->on('genres')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::table('author_book', function(Blueprint $table) {
            $table->foreign('author_id')->references('id')->on('authors')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::table('author_book', function(Blueprint $table) {
            $table->foreign('book_id')->references('id')->on('book')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('props_type_values', function(Blueprint $table) {
            $table->dropForeign('props_type_values_type_id_foreign');
        });
        Schema::table('props_type_values', function(Blueprint $table) {
            $table->dropForeign('props_type_values_book_id_foreign');
        });
        Schema::table('book_tag', function(Blueprint $table) {
            $table->dropForeign('book_tag_book_id_foreign');
        });
        Schema::table('book_tag', function(Blueprint $table) {
            $table->dropForeign('book_tag_tag_id_foreign');
        });
        Schema::table('book_genre', function(Blueprint $table) {
            $table->dropForeign('book_genre_book_id_foreign');
        });
        Schema::table('book_genre', function(Blueprint $table) {
            $table->dropForeign('book_genre_genre_id_foreign');
        });
        Schema::table('author_book', function(Blueprint $table) {
            $table->dropForeign('author_book_author_id_foreign');
        });
        Schema::table('author_book', function(Blueprint $table) {
            $table->dropForeign('author_book_book_id_foreign');
        });
    }
}
