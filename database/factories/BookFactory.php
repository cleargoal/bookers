<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use Faker\Generator as Faker;

$words = include_once 'database\seeds\words.php';
$factory->define(
    Book::class, function (Faker $faker) use($words) {
        return [
            'category_id' => rand(1, 171), // костыль - надо считать кол-во категорий
            'name' => ucfirst(implode(' ',Arr::random($words, $count = rand(3,5)))),
            'annotation' => $faker->realText($maxNbChars = 250, $indexSize = 3),
            'isbn' => $faker->ean13(),
            'publisher_id' => rand(10, 120),
            'publish_year' => rand(1900, 2020),
            'citation' => $faker->realText($maxNbChars = 250, $indexSize = 3),
            'pages' => rand(30, 5000),
            'chapters' => rand(1, 25),
        ];
    }
);
