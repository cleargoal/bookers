<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cats = include_once 'database\seeds\categories.php';
        foreach ($cats as $cat => $content) {
            if (is_array($content)) {
                $categModel = factory(App\Category::class, 1)->make()[0];
                $categModel->name = $cat;
                $categModel->save();
                $modelId = $categModel->id;
                foreach ($content as $value) {
                    $categModel = factory(App\Category::class, 1)->make()[0];
                    $categModel->name = $value;
                    $categModel->parent_id = $modelId;
                    $categModel->save();
                }
            }
        }
    }


}
