<?php

use Illuminate\Database\Seeder;

class CardBookStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = file('database/seeds/book_states.txt', FILE_IGNORE_NEW_LINES);
        foreach ($states as $state) {
            $someState = new \App\CardBookState();
            $someState->name = $state;
            $someState->save();
        }
    }
}
