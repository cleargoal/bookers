<?php

use Illuminate\Database\Seeder;

class PropsTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $propsTypes = file('database/seeds/props_types.txt', FILE_IGNORE_NEW_LINES);
        foreach ($propsTypes as $type) {
            $someType = new \App\PropsType();
            $someType->name = $type;
            $someType->save();
        }
    }
}
