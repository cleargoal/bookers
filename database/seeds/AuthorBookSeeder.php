<?php

use Illuminate\Database\Seeder;

class AuthorBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bookIds = \App\Book::all()->pluck('id');
        $authorIds = \App\Author::all()->pluck('id');

        foreach ($bookIds as $bookId) {
            $ab = new \App\AuthorBook();
            $ab->book_id = $bookId;
            $ab->author_id = $authorIds->random();
            $ab->save();
        }

        for ($i = 1; $i <= 500; $i++) {
            $ab = new \App\AuthorBook();
            $ab->book_id = $bookIds->random();
            $ab->author_id = $authorIds->random();
            $attAuthors = \App\Book::find($ab->book_id)->authors;
            if (!$attAuthors->contains($ab->author_id)) {
                $ab->save();
            }
        }

        for ($i = 1; $i <= 100; $i++) {
            $ab = new \App\AuthorBook();
            $ab->book_id = $bookIds->random();
            $ab->author_id = $authorIds->random();
            $attAuthors = \App\Book::find($ab->book_id)->authors;
            if (!$attAuthors->contains($ab->author_id)) {
                $ab->save();
            }
        }

    }
}
