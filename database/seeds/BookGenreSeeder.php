<?php

use Illuminate\Database\Seeder;

class BookGenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genreIds = \App\Genre::all()->pluck('id');

        \App\Book::chunk(
            200,
            function ($books) use ($genreIds) {
                echo 'chunk N: ' . PHP_EOL;
                $booksIds = $books->pluck('id');
                foreach ($booksIds as $bookId) {
                    $ranAmount = rand(1, 6);
                    $randomGenres = $genreIds->random($ranAmount);
                    foreach ($randomGenres as $rg) {
                        $justBG = new \App\BookGenre();
                        $justBG->book_id = $bookId;
                        $justBG->genre_id = $rg;
                        $justBG->save();
                    }
                }
            }
        );
    }
}
