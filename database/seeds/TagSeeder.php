<?php

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Book::chunk(
            200,
            function ($books) {
                echo 'chunk N: ' .  PHP_EOL;
                foreach ($books as $book) {
                    $citeCollect = Str::of(str_replace('--', ' ', $book->citation))->explode(' ');
                    $tags = $citeCollect->map(
                        function ($elem) {
                            $symbols = [
                                '.',
                                ',',
                                '\'',
                                '"',
                                '\\',
                                '\/',
                                ':',
                                ';',
                                '!',
                                '?',
                                '#',
                                '$',
                                '%',
                                '*',
                                '(',
                                ')',
                            ];
                            $elemReady = Str::lower(str_replace($symbols, '', $elem));
                            return [
                                'length' => Str::of($elemReady)->length(),
                                'name' => $elemReady,
                            ];
                        }
                    )->sortByDesc('length')->take(5)->pluck('name');
                    $tagFound = \App\Tag::whereIn('name', $tags)->get();
                    foreach ($tagFound as $found) {
                        $this->saveBookTagRelations($found, $book);
                    }
                    $notSaved = $tags->diff($tagFound->pluck('name'));
                    $this->saveTagsToDB($notSaved, $book);
                }
            }
        );
    }

    protected function saveTagsToDB($notSaved, $book)
    {
        foreach ($notSaved as $item) {
            $tag = new \App\Tag();
            $tag->name = $item;
            $tag->save();
            $this->saveBookTagRelations($tag, $book);
        }
    }

    protected function saveBookTagRelations($tag, $book)
    {
        $bookTag = new \App\BookTag();
        $bookTag->book_id = $book->id;
        $bookTag->tag_id = $tag->id;
        $bookTag->save();
    }
}
