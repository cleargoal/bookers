<?php
namespace seeds;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(UserSeeder::class);
         $this->call( \AuthorSeeder::class);
         $this->call( \PublisherSeeder::class);
         $this->call( \CategorySeeder::class);
         $this->call( \GenreSeeder::class);
         $this->call( \PropsTypeSeeder::class);
         $this->call( \UserSeeder::class);
         $this->call( \BookSeeder::class);
         $this->call( \TagSeeder::class); // also seeds into relational table 'book_tag' instead of BookTagSeeder
         $this->call( \AuthorBookSeeder::class);
         $this->call( \BookGenreSeeder::class);
    }
}
