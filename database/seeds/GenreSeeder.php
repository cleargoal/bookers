<?php

use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genres = file('database/factories/genres_src.txt', FILE_IGNORE_NEW_LINES);
        sort($genres);
        foreach ($genres as $genre) {
            $newGenre = new \App\Genre();
            $newGenre->name = $genre;
            $newGenre->save();
        }
    }
}
