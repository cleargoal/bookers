{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('name', 'Name:') !!}
			{!! Form::text('name') !!}
		</li>
		<li>
			{!! Form::label('annotation', 'Annotation:') !!}
			{!! Form::text('annotation') !!}
		</li>
		<li>
			{!! Form::label('isbn', 'Isbn:') !!}
			{!! Form::text('isbn') !!}
		</li>
		<li>
			{!! Form::label('publisher_id', 'Publisher_id:') !!}
			{!! Form::text('publisher_id') !!}
		</li>
		<li>
			{!! Form::label('publish_year', 'Publish_year:') !!}
			{!! Form::text('publish_year') !!}
		</li>
		<li>
			{!! Form::label('citation', 'Citation:') !!}
			{!! Form::textarea('citation') !!}
		</li>
		<li>
			{!! Form::label('pages', 'Pages:') !!}
			{!! Form::text('pages') !!}
		</li>
		<li>
			{!! Form::label('chapters', 'Chapters:') !!}
			{!! Form::text('chapters') !!}
		</li>
		<li>
			{!! Form::label('category_id', 'Category_id:') !!}
			{!! Form::text('category_id') !!}
		</li>
		<li>
			{!! Form::label('picture', 'Picture:') !!}
			{!! Form::text('picture') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}