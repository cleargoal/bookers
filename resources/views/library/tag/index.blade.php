@extends('layouts.app')
@section('content')

    <section class="section">
        <div class="container has-text-centered">
            <h2 class="title">Tags</h2>
            <p class="subtitle">The tags of this mountains of books</p>
            <div class="columns is-multiline">
                @foreach($tags as $tag)
                    <div class="tags column">
                        <a href="{{route('tag.show', $tag->id)}}">
                            <span class="tag is-small is-link is-light">{{$tag['name']}}</span>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div>{{$tags->links()}}</div>
    </section>
@endsection
