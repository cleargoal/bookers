@extends('layouts.app')
@section('content')

    <section class="section">
        <div class="container">
            @php $bCount = $books->count();
                $left = ceil($bCount/2);
                $right = $bCount - $left;
            @endphp
            <h2 class="title">Books by Tag "{{$tag->name}}": {{$bCount}}</h2>
            <div class="columns">

                <div class="column is-6">
                    <div class="columns is-mobile is-multiline">
                        <ol>
                            @foreach($books as $key => $book)
                                <div class="column is-4-mobile is-12-tablet">
                                    <li class="ml-5"><a href="{{route('book.show', $book->id)}}">
                                            <h4 class="title is-5 has-text-primary">{{$book->name}}</h4></a></li>
                                    <p>{{$book->annotation}}</p>
                                </div>
                                @if($key == $left -1)
                                    @break
                                @endif
                            @endforeach
                        </ol>
                    </div>
                </div>

                <div class="column is-6">
                    <div class="columns is-mobile is-multiline">
                        <ol start="{{$left + 1}}">
                            @foreach($books as $key => $book)
                                @if($key < $left)
                                    @continue
                                @endif
                                <div class="column is-4-mobile is-12-tablet">
                                    <li class="ml-5"><a href="{{route('book.show', $book->id)}}"><h4
                                                class="title is-5 has-text-primary">{{$book->name}}</h4></a></li>
                                    <p>{{$book->annotation}}</p>
                                </div>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
            <div class="columns">
            </div>
        </div>
    </section>
@endsection
