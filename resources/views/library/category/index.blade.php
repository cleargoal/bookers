@extends('layouts.app')
@section('content')

    <section class="section">
        <div class="container has-text-centered">
            <h2 class="title">Categories</h2>
            <p class="subtitle">The categories of this mountains of books</p>
            <div class="columns is-multiline">
                @foreach($categories as $category)
                    <div class="column is-2">
                        <a href="{{route('category.show', $category->id)}}">
                            <div class="level">
                                <div class="level-item">
                                    <figure class="image is-128x128">
                                        <img class="is-rounded"
                                             src="{{asset('img/bg_circle.svg?primary=00d1b2')}}"
                                             alt=""></figure>
                                </div>
                            </div>
                            <h5 class="title is-5">{{$category['name']}}</h5>
                            <h6>Books - {{$category->books->count()}}</h6>
                            <p>{{Str::limit($category['description'], 22)}}</p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div>{{$categories->links()}}</div>
    </section>
@endsection
