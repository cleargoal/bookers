@extends('layouts.app')
@section('content')

    <section class="section">
        <div class="container has-text-centered">
            <h2 class="title">Authors</h2>
            <p class="subtitle">They are the guys that made this mountains of books exist</p>
            <div class="columns is-multiline">
                @foreach($authors as $author)
                    <div class="column is-2">
                        <a href="{{route('author.show', $author->id)}}">
                            <div class="level">
                                <div class="level-item">
                                    <figure class="image is-128x128">
                                        <img class="is-rounded"
                                             src="{{asset('img/bg_circle.svg?primary=00d1b2')}}"
                                             alt=""></figure>
                                </div>
                            </div>
                            <h5 class="title is-5">{{$author['name']}}</h5>
                            <h6>Books - {{$author->books->count()}}</h6>
                            <p>{{Str::limit($author['description'], 50)}}</p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div>{{$authors->links()}}</div>
    </section>
@endsection
