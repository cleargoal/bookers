@extends('layouts.app')
@section('content')

    <section class="section">
        <div class="container has-text-centered">
            <h2 class="title">Publishers</h2>
            <p class="subtitle">They are the guys that made this mountains of books published</p>
            <div class="columns is-multiline">
                @foreach($publishers as $publisher)
                    <div class="column is-2">
                        <a href="{{route('publisher.show', $publisher->id)}}">
                            <div class="level">
                                <div class="level-item">
                                    <figure class="image is-128x128">
                                        <img class="is-rounded"
                                             src="{{asset('img/bg_circle.svg?primary=00d1b2')}}"
                                             alt=""></figure>
                                </div>
                            </div>
                            <h5 class="title is-5">{{$publisher['name']}}</h5>
                            <h6>Books - {{$publisher->books->count()}}</h6>
                            <p>{{Str::limit($publisher['description'], 50)}}</p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div>{{$publishers->links()}}</div>
    </section>
@endsection
