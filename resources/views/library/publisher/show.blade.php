@extends('layouts.app')
@section('content')

    <section class="section">
        <div class="container">
            <h2 class="title">Publisher: {{$publisher->name}}</h2>
            <div class="columns">
                <div class="column is-6">
                    <a href="#"><img src="https://bulma.dev/placeholder/pictures/bg_4-3.svg?primary=00d1b2" alt=""></a>
                    <div class="column">
                        <h3 class="title is-spaced is-4">{{$publisher->name}}</h3>
                        <p class="subtitle">{{$publisher->description}}</p>
                    </div>
                </div>


                <div class="column is-6">
                    <h3 class="title is-spaced is-4">Books him created ({{$books->count()}})</h3>
                    <div class="columns is-mobile is-multiline">
                        <ol>
                            @foreach($books as $book)
                                <div class="column is-4-mobile is-12-tablet">
                                    <li class="ml-5"><a href="{{route('book.show', $book->id)}}"><h4 class="title is-5 has-text-primary">{{$book->name}}</h4></a></li>
                                    <p>{{$book->annotation}}</p>
                                </div>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
            <div class="columns">
            </div>
        </div>
    </section>
@endsection
