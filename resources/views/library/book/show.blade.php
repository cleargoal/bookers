@extends('layouts.app')
@section('content')

    <section class="section">
        <div class="container">
            <h2 class="title">Book: {{$book->name}}</h2>
            <h4 class="tag is-link is-medium">ISBN: {{$book->isbn}}</h4>
            <div class="columns">
                <div class="column is-8">
                    <a href="#"><img src="https://bulma.dev/placeholder/pictures/bg_4-3.svg?primary=00d1b2" alt=""></a>
                    <div class="column">
                        <h3 class="title is-spaced is-4">{{$book->name}}</h3>
                        <h4 class="tag is-primary is-medium">Annotation:</h4>
                        <p class="subtitle">{{$book->annotation}}</p>
                        <h4 class="tag is-primary is-medium">Citation:</h4>
                        <p class="subtitle">{{$book->citation}}</p>
                    </div>
                </div>


                <div class="column is-4">
                    <h3 class="title is-spaced is-4">{{$theAuthors}}</h3>
                    <div class="columns is-mobile is-multiline">
                        <ol>
                            @foreach($authors as $author)
                                <div class="column is-4-mobile is-12-tablet">
                                    <li class="ml-5">
                                        <a href="{{route('author.show', $author->id)}}">
                                            <h4 class="title is-5 has-text-primary">{{$author->name}}</h4>
                                        </a>
                                    </li>
                                    <p>{{Str::limit($author->description, 50)}}</p>
                                </div>
                            @endforeach
                        </ol>
                    </div>

                    <h3 class="title is-spaced is-4">Publisher</h3>
                    <div class="columns is-mobile is-multiline">
                        <div class="column is-4-mobile is-12-tablet">
                            <a href="{{route('publisher.show', $publisher->id)}}"><h4
                                    class="title is-5 has-text-primary">{{$publisher->name}}</h4></a>
                            <p>{{Str::limit($publisher->description, 50)}}</p>
                        </div>
                    </div>

                    <h3 class="title is-spaced is-4">Published in <span
                            class="tag is-large">{{$book->publish_year}}</span></h3>
                    <h3 class="title is-spaced is-4">Chapters/pages <span class="tag is-medium">{{$book->chapters}} / {{$book->pages}}</span>
                    </h3>

                    <h3 class="title is-spaced is-4">Category</h3>
                    <div class="columns is-mobile is-multiline">
                        <div class="column is-4-mobile is-12-tablet">
                            <a href="{{route('category.show', $category->id)}}"><h4
                                    class="title is-5 has-text-primary">{{$category->name}}</h4></a>
                            <p>{{Str::limit($category->description, 50)}}</p>
                        </div>
                    </div>

                    <h3 class="title is-spaced is-4">{{$theGenres}}</h3>
                    <div class="tags" style="justify-content: space-between;">
                        @foreach($genres as $genre)
                            <a href="{{route('genre.show', $genre->id)}}">
                                <span class="tag is-primary is-medium">{{$genre->name}}</span>
                            </a>
{{--                            <p>{{Str::limit($genre->description, 20)}}</p>--}}
                        @endforeach
                    </div>

                    <h3 class="title is-spaced is-4">Tags</h3>
                    <div class="tags" style="justify-content: space-between;">
                        @foreach($tags as $tag)
                            <a href="{{route('tag.show', $tag->id)}}">
                                <span class="tag is-link is-light">{{$tag->name}}</span>
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="columns">
                </div>
            </div>
        </div>
    </section>
@endsection
