@extends('layouts.app')
@section('content')

    <section class="section">
        <div class="container has-text-centered">
            <h2 class="title">Books</h2>
            <p class="subtitle">This mountains of books</p>
            <div class="columns is-multiline">
                @foreach($books as $book)
                    <div class="column is-2">
                        <a href="{{route('book.show', $book->id)}}">
                            <div class="level">
                                <div class="level-item">
                                    <figure class="image is-128x128">
                                        <img class="is-rounded"
                                             src="{{asset('img/bg_circle.svg?primary=00d1b2')}}"
                                             alt=""></figure>
                                </div>
                            </div>
                            <h5 class="title is-5">{{$book['name']}}</h5>
                            {{--                        <p class="subtitle is-6">CEO</p>--}}
                            <p>{{Str::limit($book['annotation'], 50)}}</p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div>{{$books->links()}}</div>
    </section>
@endsection
