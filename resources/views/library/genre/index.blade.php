@extends('layouts.app')
@section('content')

    <section class="section">
        <div class="container has-text-centered">
            <h2 class="title">Genres</h2>
            <p class="subtitle">Genres of this mountains of books</p>
            <div class="columns is-multiline">
                @foreach($genres as $genre)
                    <div class="column is-2">
                        <a href="{{route('genre.show', $genre->id)}}">
                            <div class="level">
                                <div class="level-item">
                                    <figure class="image is-128x128">
                                        <img class="is-rounded"
                                             src="{{asset('img/bg_circle.svg?primary=00d1b2')}}"
                                             alt=""></figure>
                                </div>
                            </div>
                            <h5 class="title is-5">{{$genre['name']}}</h5>
                            <h6>Books - {{$genre->books->count()}}</h6>
                            <p>{{Str::limit($genre['description'], 50)}}</p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div>{{$genres->links()}}</div>
    </section>
@endsection
