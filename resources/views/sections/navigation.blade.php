<div class="container is-fluid">

    <nav class="navbar">
        <div class="container">
            <div class="navbar-brand"><a class="navbar-item" href="/">Lara Book</a><a class="navbar-burger"
                                                                                      role="button" aria-label="menu"
                                                                                      aria-expanded="false"><span
                        aria-hidden="true"></span><span aria-hidden="true"></span><span aria-hidden="true"></span></a>
            </div>
            <div class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item" href="#">Features</a>
                    <a class="navbar-item" href="#">Enterprise</a>
                    <a class="navbar-item" href="#">Support</a>
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">Views</a>
                        <div class="navbar-dropdown">
                            <a class="navbar-item navbar-item-dropdown" href="{{route('author.index')}}">Authors</a>
                            <a class="navbar-item navbar-item-dropdown" href="{{route('book.index')}}">Books</a>
                            <a class="navbar-item navbar-item-dropdown"
                               href="{{route('category.index')}}">Categories</a>
                            <a class="navbar-item navbar-item-dropdown"
                               href="{{route('publisher.index')}}">Publishers</a>
                            <a class="navbar-item navbar-item-dropdown" href="{{route('genre.index')}}">Genres</a>
                            <a class="navbar-item navbar-item-dropdown" href="{{route('tag.index')}}">Tags</a>
                        </div>
                    </div>
                </div>
                <div class="navbar-end">
                    <div class="navbar-item">
                        <div class="buttons">
                            @guest
                                <a class="button is-light" href="{{ route('login') }}">Log in</a>
                                @if (Route::has('register'))
                                    <a class="button is-primary" href="{{ route('register') }}">Sign up</a>
                                @endif
                            @else
                                <a href="#">{{ Auth::user()->name }}</a>
                            @endguest
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>

{{--
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>--}}
