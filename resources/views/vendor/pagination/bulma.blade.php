@if ($paginator->hasPages())
    <nav class="mt-6">
        <div class="container">
            <div class="pagination is-centered" role="navigation" aria-label="pagination">
                @if ($paginator->onFirstPage())
                    <a class="pagination-previous" disabled>Previous</a>
                @else
                    <a href="{{ $paginator->previousPageUrl() }}" class="pagination-previous">Previous</a>
                @endif


                @if ($paginator->hasMorePages())
                    <a href="{{ $paginator->nextPageUrl() }}" class="pagination-next">Next page</a>
                @else
                    <a class="pagination-next" disabled>Next page</a>
                @endif

                <ul class="pagination-list">
                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li><a class="pagination-link is-current" aria-label="Goto page {{ $page }}">{{ $page }}</a>
                                    </li>
                                @else
                                    <li><a href="{{ $url }}" class="pagination-link"
                                           aria-label="Goto page {{ $page }}">{{ $page }}</a></li>
                                @endif
                            @endforeach
                        @endif
                        {{--                        <li><span class="pagination-ellipsis">&hellip;</span></li>          --}}
                    @endforeach
                </ul>

            </div>
        </div>
    </nav>
@endif
