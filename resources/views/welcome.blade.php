@extends('layouts.app')
@section('content')

    <section class="section">
        <div class="container has-text-centered">
            <h2 class="title">Look at our awesome library</h2>
            <div class="block">
                <img class="is-16by9" src="{{asset('img/title-book.png')}}" alt="title book">
            </div>

            <div class="columns has-text-left">
                @foreach($topics as $topic)
                    <div class="column is-2">
                        <a href="{{$topic['route']}}">
                            <div class="columns">
                                <div class="column is-2"><img src="{{asset('img/' . $topic['img'])}}" alt="">
                                </div>
                                <div class="column is-10">
                                    <h4 class="title is-spaced is-4">{{$topic['label']}}</h4>
                                    <p class="subtitle is-6">{{$topic['text']}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

            <div class="buttons is-centered">
                <a class="button is-primary" href="https://github.com/laravel/laravel">My reader's card</a>
                <a class="button is-warning" href="https://github.com/laravel/laravel">Find books</a>
                <a class="button is-family-secondary" href="https://github.com/laravel/laravel" target="_blank">GitLab</a>
            </div>
        </div>
    </section>

@endsection
