<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    protected $table = 'books';
    public $timestamps = true;
    protected $fillable = array('name', 'annotation', 'isbn', 'publisher_id', 'publish_year', 'citation', 'pages', 'chapters', 'category_id', 'picture');

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function authors()
    {
        return $this->belongsToMany('App\Author');
    }

    public function genres()
    {
        return $this->belongsToMany('App\Genre');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function values()
    {
        return $this->hasMany('App\PropsTypeValue');
    }

    public function publisher()
    {
        return $this->belongsTo('App\Publisher');
    }

    public function cards()
    {
        return $this->belongsToMany('App\ReaderCard');
    }
}
