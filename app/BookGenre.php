<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookGenre extends Model
{
    protected $table = 'book_genre';
    public $timestamps = false;
    protected $primaryKey = null;
    public $incrementing = false;
}
