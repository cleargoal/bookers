<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{

    protected $table = 'genres';
    public $timestamps = true;
    protected $fillable = array('name', 'description');

    public function books()
    {
        return $this->belongsToMany('App\Book');
    }

}
