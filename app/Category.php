<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';
    public $timestamps = true;
    protected $fillable = array('name', 'description', 'parent_id', 'picture');

    public function parent()
    {
        return $this->belongsTo('Category', 'id');
    }

    public function books()
    {
        return $this->hasMany('App\Book');
    }

}
