<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReaderCard extends Model
{

    protected $table = 'reader_cards';
    public $timestamps = true;
    protected $fillable = array('book_id', 'user_id');
    protected $visible = array('state_id');

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function bookStates()
    {
        return $this->belongsToMany('CardBookStates', 'state_id');
    }

    public function books()
    {
        return $this->belongsToMany('Book', 'book_id');
    }

}
