<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{

    protected $table = 'authors';
    public $timestamps = true;
    protected $fillable = array('name', 'description', 'avatar');

    public function books()
    {
        return $this->belongsToMany('App\Book');
    }

    public function categoryByBook()
    {
        return \App\Category::whereIn('id', $this->books->pluck('category_id'))->orderBy('name')->get();
    }

    public function genreByBook()
    {
        return \App\Genre::whereIn(
            'id',
            \App\BookGenre::
            whereIn('book_id', $this->books->pluck('id'))
                ->get()
                ->pluck('genre_id')
        )
            ->orderBy('name')
            ->get();
    }

}
