<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropsType extends Model
{

    protected $table = 'props_types';
    public $timestamps = true;
    protected $fillable = array('name');

    public function values()
    {
        return $this->hasMany('PropsTypeValue');
    }

}
