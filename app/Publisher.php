<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{

    protected $table = 'publishers';
    public $timestamps = true;
    protected $fillable = array('name', 'description', 'avatar');

    public function books()
    {
        return $this->hasMany('App\Book');
    }

}
