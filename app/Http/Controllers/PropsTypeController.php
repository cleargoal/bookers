<?php

namespace App\Http\Controllers;

use App\PropsType;
use Illuminate\Http\Request;

class PropsTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PropsType  $propsType
     * @return \Illuminate\Http\Response
     */
    public function show(PropsType $propsType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PropsType  $propsType
     * @return \Illuminate\Http\Response
     */
    public function edit(PropsType $propsType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PropsType  $propsType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PropsType $propsType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PropsType  $propsType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropsType $propsType)
    {
        //
    }
}
