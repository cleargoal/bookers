<?php

namespace App\Http\Controllers;

use App\ReaderCard;
use Illuminate\Http\Request;

class ReaderCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReaderCard  $readerCard
     * @return \Illuminate\Http\Response
     */
    public function show(ReaderCard $readerCard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReaderCard  $readerCard
     * @return \Illuminate\Http\Response
     */
    public function edit(ReaderCard $readerCard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReaderCard  $readerCard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReaderCard $readerCard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReaderCard  $readerCard
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReaderCard $readerCard)
    {
        //
    }
}
