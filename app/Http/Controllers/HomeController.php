<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /** Welcome
     *
     * @return Factory|View
     */
    public function welcome()
    {
        $topics = collect(
            [
                'authors' => [
                    'label' => 'Authors',
                    'img' => 'check.svg',
                    'route' => 'author',
                    'text' => "These are authors who's book we present",
                ],
                'book' => [
                    'label' => 'Books',
                    'img' => 'check.svg',
                    'route' => 'book',
                    'text' => "These are book that we present",
                ],
                'categories' => [
                    'label' => 'Categories',
                    'img' => 'check.svg',
                    'route' => 'category',
                    'text' => "These are categories we present the book in",
                ],
                'publishers' => [
                    'label' => 'Publishers',
                    'img' => 'check.svg',
                    'route' => 'publisher',
                    'text' => "These are publishers who's book we present",
                ],
                'genres' => [
                    'label' => 'Genres',
                    'img' => 'check.svg',
                    'route' => 'genre',
                    'text' => "These are genres we present the book in",
                ],
                'tags' => [
                    'label' => 'Tags',
                    'img' => 'check.svg',
                    'route' => 'tag',
                    'text' => "These are tags of book we present",
                ],
            ]
        );
        return view('welcome', ['topics' => $topics]);
    }
}
