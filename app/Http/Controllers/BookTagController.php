<?php

namespace App\Http\Controllers;

use App\BookTag;
use Illuminate\Http\Request;

class BookTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookTag  $bookTag
     * @return \Illuminate\Http\Response
     */
    public function show(BookTag $bookTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookTag  $bookTag
     * @return \Illuminate\Http\Response
     */
    public function edit(BookTag $bookTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookTag  $bookTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookTag $bookTag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookTag  $bookTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookTag $bookTag)
    {
        //
    }
}
