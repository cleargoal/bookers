<?php

namespace App\Http\Controllers;

use App\CardBookState;
use Illuminate\Http\Request;

class CardBookStateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CardBookState  $cardBookState
     * @return \Illuminate\Http\Response
     */
    public function show(CardBookState $cardBookState)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CardBookState  $cardBookState
     * @return \Illuminate\Http\Response
     */
    public function edit(CardBookState $cardBookState)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CardBookState  $cardBookState
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CardBookState $cardBookState)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CardBookState  $cardBookState
     * @return \Illuminate\Http\Response
     */
    public function destroy(CardBookState $cardBookState)
    {
        //
    }
}
