<?php

namespace App\Http\Controllers;

use App\PropsTypeValue;
use Illuminate\Http\Request;

class PropsTypeValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PropsTypeValue  $propsTypeValue
     * @return \Illuminate\Http\Response
     */
    public function show(PropsTypeValue $propsTypeValue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PropsTypeValue  $propsTypeValue
     * @return \Illuminate\Http\Response
     */
    public function edit(PropsTypeValue $propsTypeValue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PropsTypeValue  $propsTypeValue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PropsTypeValue $propsTypeValue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PropsTypeValue  $propsTypeValue
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropsTypeValue $propsTypeValue)
    {
        //
    }
}
