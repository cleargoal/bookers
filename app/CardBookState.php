<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardBookState extends Model
{

    protected $table = 'card_book_states';
    public $timestamps = true;
    protected $fillable = array('name');

    public function cards()
    {
        return $this->belongsToMany('ReaderCard', 'id');
    }

}
