<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookTag extends Model
{
    protected $table = 'book_tag';
    public $timestamps = false;
    protected $primaryKey = null;
    public $incrementing = false;
}
