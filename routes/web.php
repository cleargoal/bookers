<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [\App\Http\Controllers\HomeController::class, 'welcome'])->name('welcome-route');

Route::resource('user', 'UserController');
Route::resource('author', 'AuthorController');
Route::resource('book', 'BookController');
Route::resource('category', 'CategoryController');
Route::resource('genre', 'GenreController');
Route::resource('tag', 'TagController');
Route::resource('propstype', 'PropsTypeController');
Route::resource('propstypevalues', 'PropsTypeValueController');
Route::resource('readercard', 'ReaderCardController');
Route::resource('publisher', 'PublisherController');
Route::resource('booktag', 'BookTagController');
Route::resource('bookgenre', 'BookGenreController');
Route::resource('authorbook', 'AuthorBookController');
Route::resource('cardbookstates', 'CardBookStateController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
